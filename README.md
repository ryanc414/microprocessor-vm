Microprocesser VM
=================

Virtual implementation of a basic microprocessor with reduced instruction set.

The processor has a single 8-bit register and can address 16 bits of RAM.
Addresses are expected to be stored in little-endian byte order. The following
instruction set is supported:

| Operation            | Code | Mnemonic |
| -------------------- | ---- | -------- |
| Load                 | 0x10 | LOD      |
| Store                | 0x11 | STO      |
| Add                  | 0x20 | ADD      |
| Subtract             | 0x21 | SUB      |
| Add with Carry       | 0x22 | ADC      |
| Subtract with Borrow | 0x23 | SBB      |
| Jump                 | 0x30 | JMP      |
| Jump If Zero         | 0x31 | JZ       |
| Jump If Carry        | 0x32 | JC       |
| Jump If Not Zero     | 0x33 | JNZ      |
| Jump If Not Carry    | 0x34 | JNC      |
| Halt                 | 0xFF | HLT      |

Limitations / Improvements
==========================

This virtual processor is missing many features of a modern microprocessor.
Here are some extra features which could be added:

- Stack management - addition of a stack pointer and PUSH/POP opcodes to push
  and pop registers onto the stack.

- Assembler - a separate program to compile assembly-language instructions
  into machine code.

- Opcodes for integer multiplication and division as well as floating-point
  arithmetic and bitwise operations.

- Operating system to manage memory, provide filesystem access etc.

- Input/Output - reading input from peripherals (keyboard, mouse etc.) and
  writing output into a graphical display.

