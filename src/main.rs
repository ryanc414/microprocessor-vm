use byteorder::{LittleEndian, ReadBytesExt};
use std::env;
use std::fs;
use std::io;
use std::io::{Cursor, Write};

fn main() {
    let filename = parse_args();
    let input = load_input(&filename).unwrap();

    let mut processor = Processor::new();
    processor.load_memory(input);

    match processor.run() {
        Ok(_) => println!("Program exited successfully"),
        Err(e) => println!("Program exited with error: {}", e),
    }

    write_memory(&processor.memory).unwrap();
}

fn parse_args() -> String {
    let mut args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("Usage: {} <input file>", args[0]);
    }

    args.remove(1)
}

fn load_input(filename: &str) -> Result<Vec<u8>, String> {
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let parts: Result<Vec<Vec<u8>>, String> =
        contents.lines().map(parse_line).collect();

    match parts {
        Ok(parts) => Ok(parts.concat()),
        Err(e) => Err(e),
    }
}

fn parse_line(line: &str) -> Result<Vec<u8>, String> {
    let line = remove_comments(line);
    let line = remove_0x_prefix(line);
    hex::decode(line).map_err(|e| e.to_string())
}

fn remove_0x_prefix(line: &str) -> &str {
    match line.find("0x") {
        Some(index) => &line[index + 2..],
        None => line,
    }
}

fn remove_comments(line: &str) -> &str {
    match line.find("//") {
        Some(index) => line[..index].trim(),
        None => line,
    }
}

fn write_memory(memory: &[u8]) -> Result<(), io::Error> {
    let mut file = fs::File::create("output.txt")?;

    for (i, byte) in memory.iter().enumerate() {
        if i % 16 == 0 {
            writeln!(file)?;
        }
        write!(file, "{:02x} ", byte).unwrap();
    }

    writeln!(file)?;
    println!("Memory written to output.txt");

    Ok(())
}

struct Processor {
    memory: Vec<u8>,
    pc: usize,
    accumulator: u8,
    carry: bool,
}

impl Processor {
    fn new() -> Processor {
        Processor {
            memory: vec![0; usize::pow(2, 16)],
            pc: 0,
            accumulator: 0,
            carry: false,
        }
    }

    fn load_memory(&mut self, input: Vec<u8>) {
        for (i, byte) in input.iter().enumerate() {
            self.memory[i] = *byte;
        }
    }

    fn run(&mut self) -> Result<(), String> {
        loop {
            let opcode = self.memory[self.pc];
            match opcode {
                0x10 => self.load()?,
                0x11 => self.store()?,
                0x20 => self.add()?,
                0x21 => self.sub()?,
                0x22 => self.add_carry()?,
                0x23 => self.sub_borrow()?,
                0x30 => self.jump()?,
                0x31 => self.jump_if_zero()?,
                0x32 => self.jump_if_carry()?,
                0x33 => self.jump_if_not_zero()?,
                0x34 => self.jump_if_not_carry()?,
                0xff => return Ok(()),
                _ => return Err(format!("Invalid opcode: {:#x}", opcode)),
            }
        }
    }

    fn load(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("LOD {:#x} ({:#x})", address, self.memory[address]);

        self.accumulator = self.memory[address];
        self.pc += 3;
        Ok(())
    }

    fn store(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("STO {:#x} ({:#x})", address, self.accumulator);

        self.memory[address] = self.accumulator;
        self.pc += 3;
        Ok(())
    }

    fn add(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("ADD {:#x} ({:#x})", address, self.memory[address]);

        (self.accumulator, self.carry) = self.add_carry_common(self.memory[address], false);
        self.pc += 3;
        Ok(())
    }

    fn sub(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("SUB {:#x} ({:#x})", address, self.memory[address]);

        let value = !self.memory[address];
        (self.accumulator, self.carry) = self.add_carry_common(value, true);
        self.pc += 3;
        Ok(())
    }

    fn add_carry(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("ADC {:#x} ({:#x})", address, self.memory[address]);

        (self.accumulator, self.carry) = self.add_carry_common(self.memory[address], self.carry);
        self.pc += 3;
        Ok(())
    }

    fn sub_borrow(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("SBB {:#x} ({:#x})", address, self.memory[address]);

        let value = !self.memory[address];
        (self.accumulator, self.carry) = self.add_carry_common(value, self.carry);
        self.pc += 3;
        Ok(())
    }

    fn add_carry_common(&self, value: u8, carry: bool) -> (u8, bool) {
        let (value, carry1) = if carry {
            value.overflowing_add(1)
        } else {
            (value, false)
        };

        let (accumulator, carry2) = self.accumulator.overflowing_add(value);
        (accumulator, carry1 || carry2)
    }

    fn jump(&mut self) -> Result<(), String> {
        let address = self.read_address()? as usize;
        println!("JMP {:#x}", address);

        self.pc = address;
        Ok(())
    }

    fn jump_if_zero(&mut self) -> Result<(), String> {
        if self.accumulator == 0 {
            self.jump()
        } else {
            self.pc += 3;
            Ok(())
        }
    }

    fn jump_if_carry(&mut self) -> Result<(), String> {
        if self.carry {
            self.jump()
        } else {
            self.pc += 3;
            Ok(())
        }
    }

    fn jump_if_not_zero(&mut self) -> Result<(), String> {
        if self.accumulator != 0 {
            self.jump()
        } else {
            self.pc += 3;
            Ok(())
        }
    }

    fn jump_if_not_carry(&mut self) -> Result<(), String> {
        if !self.carry {
            self.jump()
        } else {
            self.pc += 3;
            Ok(())
        }
    }

    fn read_address(&self) -> Result<u16, String> {
        let mut rdr = Cursor::new(&self.memory[self.pc + 1..self.pc + 3]);
        rdr.read_u16::<LittleEndian>().map_err(|e| e.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_16bit_add() {
        let mut p = Processor::new();

        // Add 0x76ab to 0x236c
        p.load_memory(vec![
            0x10, 0x15, 0x00, // LOD 0x0015
            0x20, 0x16, 0x00, // ADD 0x0016
            0x11, 0x19, 0x00, // STO 0x0019
            0x10, 0x17, 0x00, // LOD 0x0017
            0x22, 0x18, 0x00, // ADC 0x0018
            0x11, 0x1a, 0x00, // STO 0x001a
            0xff, 0x00, 0x00, // HLT
            0xab, // 0xab at 0x0015
            0x6c, // 0x6c at 0x0016
            0x76, // 0x76 at 0x0017
            0x23, // 0x23 at 0x0018
        ]);

        p.run().unwrap();

        assert_eq!(p.pc, 0x12);

        // 0x76ab + 0x236c = 0x9a17
        assert_eq!(p.memory[0x19], 0x17);
        assert_eq!(p.memory[0x1a], 0x9a);
    }

    #[test]
    fn test_16bit_multiply() {
        let mut p = Processor::new();

        // Note that data values in this program are stored in big-endian order,
        // unlike the addresses stored in little-endian order. How to encode
        // multi-byte values is up to the programmer.
        p.load_memory(vec![
            0x10, 0x26, 0x00, // LOD 0x0026
            0x20, 0x22, 0x00, // ADD 0x0022
            0x11, 0x26, 0x00, // STO 0x0026
            0x10, 0x25, 0x00, // LOD 0x0025
            0x22, 0x21, 0x00, // ADC 0x0021
            0x11, 0x25, 0x00, // STO 0x0025
            0x10, 0x24, 0x00, // LOD 0x0024
            0x20, 0x1e, 0x00, // ADD 0x001e
            0x11, 0x24, 0x00, // STO 0x0024
            0x33, 0x00, 0x00, // JNZ 0x0000
            0xff, 0x00, 0x00, // HLT
            0x00, 0xa7, // 16-bit multiplier starting at 0x0021
            0x00, 0x1c, // 16-bit multiplicand starting at 0x0023
            0x00, 0x00, // 16-bit product starting at 0x0025
        ]);

        p.run().unwrap();

        assert_eq!(p.pc, 0x1e);

        // 0x00a7 * 0x001c = 0x1244
        assert_eq!(p.memory[0x25], 0x12);
        assert_eq!(p.memory[0x26], 0x44);
    }

    #[test]
    fn test_load() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x10, 0x04, 0x00, // LOD 0x0004
            0xff, // HLT
            0xaa, // 0xaa at 0x0004
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0xaa);
    }

    #[test]
    fn test_store() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x11, 0x04, 0x00, // STR 0x0004
            0xff, // HLT
        ]);

        p.accumulator = 0xaa;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0xaa);
        assert_eq!(p.memory[4], 0xaa);
    }

    #[test]
    fn test_add_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x20, 0x04, 0x00, // ADD 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x03);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_add_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x20, 0x04, 0x00, // ADD 0x0004
            0xff, // HLT
            0xff, // 0xff at 0x0004
        ]);

        p.accumulator = 0x01;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x00);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_subtract_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x21, 0x04, 0x00, // SUB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x05;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x03);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_subtract_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x21, 0x04, 0x00, // SUB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0xff);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_add_carry_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x22, 0x04, 0x00, // ADC 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x03);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_add_carry_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x22, 0x04, 0x00, // ADC 0x0004
            0xff, // HLT
            0xff, // 0xff at 0x0004
        ]);

        p.accumulator = 0x01;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x00);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_add_carry_3() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x22, 0x04, 0x00, // ADC 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;
        p.carry = true;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x04);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_add_carry_4() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x22, 0x04, 0x00, // ADC 0x0004
            0xff, // HLT
            0xff, // 0xff at 0x0004
        ]);

        p.accumulator = 0x01;
        p.carry = true;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x01);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_add_carry_5() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x22, 0x04, 0x00, // ADC 0x0004
            0xff, // HLT
            0xfe, // 0xfe at 0x0004
        ]);

        p.accumulator = 0x01;
        p.carry = true;

        p.run().unwrap();
        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x00);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_subtract_borrow_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x23, 0x04, 0x00, // SBB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x05;
        p.carry = true;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x03);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_subtract_borrow_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x23, 0x04, 0x00, // SBB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;
        p.carry = true;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0xff);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_subtract_borrow_3() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x23, 0x04, 0x00, // SBB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x05;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x02);
        assert_eq!(p.carry, true);
    }

    #[test]
    fn test_subtract_borrow_4() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x23, 0x04, 0x00, // SBB 0x0004
            0xff, // HLT
            0x02, // 0x02 at 0x0004
        ]);

        p.accumulator = 0x01;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0xfe);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_subtract_borrow_5() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x23, 0x04, 0x00, // SBB 0x0004
            0xff, // HLT
            0xff, // 0xff at 0x0004
        ]);

        p.accumulator = 0x01;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
        assert_eq!(p.accumulator, 0x01);
        assert_eq!(p.carry, false);
    }

    #[test]
    fn test_jump() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x30, 0x04, 0x00, // JMP 0x0004
            0x00, // Junk
            0xff, // HLT
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 4);
    }

    #[test]
    fn test_jump_if_zero_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x31, 0x04, 0x00, // JZ 0x0004
            0x00, // Junk
            0xff, // HLT
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 4);
    }

    #[test]
    fn test_jump_if_zero_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x31, 0x04, 0x00, // JZ 0x0004
            0xff, // HLT
        ]);

        p.accumulator = 0x01;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
    }

    #[test]
    fn test_jump_if_carry_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x32, 0x04, 0x00, // JC 0x0004
            0xff, // HLT
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 3);
    }

    #[test]
    fn test_jump_if_carry_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x32, 0x04, 0x00, // JC 0x0004
            0x00, // Junk
            0xff, // HLT
        ]);

        p.carry = true;
        p.run().unwrap();
        assert_eq!(p.pc, 4);
    }

    #[test]
    fn test_jump_if_not_zero_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x33, 0x04, 0x00, // JNZ 0x0004
            0xff, // HLT
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 3);
    }

    #[test]
    fn test_jump_if_not_zero_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x33, 0x04, 0x00, // JNZ 0x0004
            0x00, // Junk
            0xff, // HLT
        ]);

        p.accumulator = 0x01;
        p.run().unwrap();

        assert_eq!(p.pc, 4);
    }

    #[test]
    fn test_jump_if_not_carry_1() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x34, 0x04, 0x00, // JNC 0x0004
            0x00, // Junk
            0xff, // HLT
        ]);

        p.run().unwrap();
        assert_eq!(p.pc, 4);
    }

    #[test]
    fn test_jump_if_not_carry_2() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x34, 0x04, 0x00, // JNC 0x0004
            0xff, // HLT
        ]);

        p.carry = true;
        p.run().unwrap();

        assert_eq!(p.pc, 3);
    }

    #[test]
    fn test_invalid_opcode() {
        let mut p = Processor::new();
        p.load_memory(vec![
            0x35, 0x04, 0x00, // Invalid opcode
            0xff, // HLT
        ]);

        let result = p.run();

        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), "Invalid opcode: 0x35");
    }
}
